#!/usr/bin/env python3
import yaml
import json
import requests
import re
import sys
from jinja2 import Template
from urllib.parse import urlparse
from helper import init_list, get_config, get_name_string, get_license_url, get_pandoc_header


config = get_config()
generate_reuse_note = config["generate_reuse_note"] if "generate_reuse_note" in config else True

text_templates = {
    "mtullu": {
        "de": "<div about=\"{{ link }}\"><a rel=\"dc:source\" href=\"{{ image_page }}\"><span property=\"dc:title\">{{ image_title }}</span></a> von <a rel=\"cc:attributionURL dc:creator\" href=\"{{ image_author_link }}\" property=\"cc:attributionName\">{{ image_author }}</a> unter <a rel=\"license\" href=\"{{ license_url }}\">{{ license_short_name }}</a></div>",
        "en": "<div about=\"{{ link }}\"><a rel=\"dc:source\" href=\"{{ image_page }}\"><span property=\"dc:title\">{{ image_title }}</span></a> by <a rel=\"cc:attributionURL dc:creator\" href=\"{{ image_author_link }}\" property=\"cc:attributionName\">{{ image_author }}</a> under <a rel=\"license\" href=\"{{ license_url }}\">{{ license_short_name }}</a></div>"
    },
    "reusage_note": {
        "de": """{% if document_license_url or  document_url %}

---
{% for n in range(reuse_note_heading_level) %}#{% endfor %} Hinweis zur Nachnutzung

{% if document_license_url %}Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben - lizenziert unter {{ document_license_short_name }}.
Nennung dieses Werkes bitte wie folgt: "[{{ document_title }}]({{ document_url }})" von {{ document_author }}, Lizenz: [{{ document_license_short_name }}]({{ document_license_url }}).{% endif %}
{% if document_url %}Die Quellen dieses Werks sind verfügbar auf [{{ domain }}]({{ document_url }}).{% endif %}
{% endif %}""",
        "en": """{% if document_license_url or  document_url %}

---
{% for n in range(reuse_note_heading_level) %}#{% endfor %} Note on reuse

{% if document_license_url %}This work and its contents are licensed under {{ document_license_short_name }} unless otherwise noted.
Please cite this work as follows: "[{{ document_title }}]({{ document_url }})" by {{ document_author }}, license: [{{ document_license_short_name }}]({{ document_license_url }}).{% endif %}
{% if document_url %}The sources of this work are available on [{{ domain }}]({{ document_url }}).{% endif %}
{% endif %}"""
    },
    "reusage_note_landingpage": {
        "de": "{% if document_license_url or  document_url %}{% if document_license_url %}Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben - lizenziert unter {{ document_license_short_name }}. Nennung dieses Werkes bitte wie folgt: {% if document_url %}<a href=\"{{ document_url }}\">{% endif %}{{ document_title }}{% if document_url %}</a>{% endif %} von {{ document_author }}, Lizenz: <a href=\"{{ document_license_url }}\">{{ document_license_short_name }}</a>. {% endif %}{% if document_url %}Die Quellen dieses Werks sind verfügbar auf <a href=\"{{ document_url }}\">{{ domain }}</a>.{% endif %}{% endif %}",
        "en": "{% if document_license_url or  document_url %}{% if document_license_url %}This work and its contents are licensed under {{ document_license_short_name }} unless otherwise noted. Please cite this work as follows: {% if document_url %}<a href=\"{{ document_url }}\">{% endif %}{{ document_title }}{% if document_url %}</a>{% endif %} by {{ document_author }}, license: <a href=\"{{ document_license_url }}\">{{ document_license_short_name }}</a>. {% endif %}{% if document_url %}The sources of this work are available on <a href=\"{{ document_url }}\">{{ domain }}</a>.{% endif %}{% endif %}"
    }
}

data = {}
with open("metadata.yml", 'r') as document_metadata:
    try:
        data = yaml.safe_load(document_metadata)
    except yaml.YAMLError as exc:
        print(exc)
        print("Cannot read metadata.yml")
        exit(1)

lngs = init_list(data, "inLanguage")
if "en" in lngs:
    output_lng = "en"
elif "de" in lngs:
    output_lng = "de"
else:
    output_lng = "en"


document_license_text = ""
if generate_reuse_note:
    document_title = data["name"]
    if "url" in data :
        document_url = data["url"]
        domain = urlparse(document_url).netloc
    else :
        document_url = ""
        domain = ""
    document_author = ", ".join(map(lambda a: get_name_string(a), init_list(data, 'creator')))

    document_license_url = get_license_url(data)
    if document_license_url:
        if "public-domain" in document_license_url or "zero" in document_license_url:
            document_license_short_name = "Public domain"
        else :
            document_license_components = re.findall("licenses\/([^\/]*)\/([^\/]*)", document_license_url)
            document_license_code = document_license_components[0][0]
            document_license_version = document_license_components[0][1]
            document_license_short_name = "CC " + document_license_code.upper() + " " + document_license_version
    else:
        document_license_short_name = None

    document_license_text = Template(text_templates["reusage_note"][output_lng]).render(
        reuse_note_heading_level=1,
        document_author=document_author,
        document_license_short_name=document_license_short_name, document_license_url=document_license_url,
        document_title=document_title, document_url=document_url,
        domain=domain
    )

    landingpage_license_text = Template(text_templates["reusage_note_landingpage"][output_lng]).render(
        reuse_note_heading_level=1,
        document_author=document_author,
        document_license_short_name=document_license_short_name, document_license_url=document_license_url,
        document_title=document_title, document_url=document_url,
        domain=domain
    )

# Add the reuse notice to the landing page
if output_lng == "de":
    with open(".template-landingpage-de.yml", "a") as template_de:
        template_de.write("\nreuse: >\n " + landingpage_license_text)
elif output_lng == "en":
    with open(".template-landingpage-en.yml", "a") as template_en:
        template_en.write("\nreuse: >\n " + landingpage_license_text)
